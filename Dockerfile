FROM mlir

RUN apt-get update -y && \
    apt-get install -y software-properties-common

RUN add-apt-repository ppa:jonathonf/vim

RUN apt-get update -y 

RUN apt-get install -y ninja-build git zsh global vim autojump \
    wget tmux cmake python3-dev ctags

    

# RUN git clone https://github.com/naver/d2codingfont.git && \
#     mkdir -p /usr/share/fonts/D2CodingFont && \
#     unzip d2codingfont/D2Coding-Ver1.3.2-20180524.zip -d /usr/share/fonts/D2CodingFont && \
#     rm -rf d2codingfont 

RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true

RUN chsh --shell /bin/zsh root

RUN git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf && \
    $HOME/.fzf/install

COPY vim /root/.vim/

RUN cd /root/.vim/pack/default/start/YouCompleteMe && \
    python3 install.py --clang-completer

COPY zsh/zsh-autosuggestions /root/.oh-my-zsh/custom/plugins/zsh-autosuggestions/

COPY zsh/zsh-syntax-highlighting /root/.zsh-syntax-highlighting/

COPY zsh/zshrc /root/.zshrc

COPY zsh/p10k.zsh /root/.p10k.zsh

COPY zsh/powerlevel10k /root/.oh-my-zsh/custom/themes/powerlevel10k

COPY tmux.conf /root/.tmux.conf

RUN DEBIAN_FRONTEND=noninteractive apt-get install -yqq tzdata

ENV TZ=Asia/Seoul

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN dpkg-reconfigure -f noninteractive tzdata




